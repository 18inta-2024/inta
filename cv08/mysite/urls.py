"""
URL configuration for mysite project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path

from diary import views

urlpatterns = [
    path("", views.index, name="index"),
    path("books/", views.BooksListView.as_view(), name="books_list"),
    path("genres/", views.GenresListView.as_view(), name="genres_list"),
    path("add_author/", views.add_author, name="add_author"),
    path("admin/", admin.site.urls),
]
