from django.contrib import admin

# import your models
from diary.models import *

admin.site.register(Genre)
admin.site.register(Author)
admin.site.register(Book)
admin.site.register(Reading)
