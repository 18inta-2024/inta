from django.shortcuts import render, HttpResponseRedirect, reverse
from django.views.generic import ListView
from django.db.models import Count

#from diary import models
from . import models
from . import forms

def index(request):
    # set rendering context - variables used in the template
    context = {}
    context["genres_count"] = models.Genre.objects.count()
    context["authors_count"] = models.Author.objects.count()
    context["books_count"] = models.Book.objects.count()
    context["readings_count"] = models.Reading.objects.count()

    # list of the newest books
    context["newest_books"] = models.Book.objects.all().order_by("-issue_date")[:5]

    # render the template
    return render(request, "diary/index.html", context)

class BooksListView(ListView):
    model = models.Book

class GenresListView(ListView):
    model = models.Genre
    queryset = models.Genre.objects.all().annotate(
                    number_of_books=Count("book")
                )

def add_author(request):
    # set rendering context - variables used in the template
    context = {}

    # check if there are input data to be processed
    if request.method == "POST":
        # create a form with data from the request
        form = forms.AuthorForm(request.POST)
        # check if the form is valid
        if form.is_valid():
            # create, validate and save the object in database
            new_author = form.save()

            # Always return an HttpResponseRedirect after successfully dealing
            # with POST data. This prevents data from being posted twice if the
            # user hits the <Back> or <Reload> button.
            return HttpResponseRedirect(reverse("index"))

    # create an empty form for GET or any other method
    else:
        form = forms.AuthorForm()

    # continue with normal rendering
    context["authors_count"] = models.Author.objects.count()
    context["form"] = form
    return render(request, "diary/add_author.html", context)
