from django.db import models
from django.contrib.sessions.models import Session

class Page(models.Model):
    path = models.CharField(max_length=1000, unique=True)
    total_visits = models.BigIntegerField(default=0)
    session_visits = models.ManyToManyField(
        Session,
        # volitelné argumenty pro pokročilé chování - on_delete
        through="PageVisit",
        through_fields=("page", "session"),
    )

class PageVisit(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
