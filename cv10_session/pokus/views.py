from django.shortcuts import render
import datetime
from . import models

def visit_counter(func):
    def wrapper(request):
        # 1. zpracování požadavku před func

        # nastavení expirace session
        request.session.set_expiry(datetime.timedelta(minutes=10))

        # uložení session v databázi a získání odpovídajícího objektu z databáze
        request.session.save()
        session_db = models.Session.objects.get(session_key=request.session.session_key)

        # vytvoření záznamu pro navštívenou stránku
        # viz https://docs.djangoproject.com/en/4.2/ref/models/querysets/#get-or-create
        page, created = models.Page.objects.get_or_create(path=request.path)
        # přiřazení návštěvy dané stránky
        page.session_visits.add(session_db)
        page.total_visits += 1
        page.save()

        # 2. použití func
        response = func(request)

        # 3. dodatečné zpracování odpovědi

        return response

    return wrapper

@visit_counter
def index(request):
    context = {
        "cookies": request.COOKIES,
        "pages": models.Page.objects.all(),
    }
    return render(request, "pokus/index.html", context)
