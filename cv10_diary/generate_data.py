first_names = [
    "Adéla",
    "Adolf",
    "Adriana",
    "Agáta",
    "Alan",
    "Alena",
    "Aleš",
    "Alexandr",
    "Alexandra",
    "Alexej",
    "Alice",
    "Alois",
    "Amálie",
    "Anastázie",
    "Anděla",
    "Andrea",
    "Andrej",
    "Aneta",
    "Anežka",
    "Anna",
    "Antonie",
    "Antonín",
    "Apolena",
    "Arnošt",
    "Augustýn",
    "Bartoloměj",
    "Beáta",
    "Bedřich",
    "Běla",
    "Bernard",
    "Berta",
    "Blahoslav",
    "Blažej",
    "Blažena",
    "Bohdana",
    "Bohumil",
    "Bohuslav",
    "Bohuslava",
    "Boleslav",
    "Bonifác",
    "Bořek",
    "Boris",
    "Bořivoj",
    "Božena",
    "Břetislav",
    "Brigita",
    "Bronislav",
    "Bruno",
    "Čeněk",
    "Čestmír",
    "Ctibor",
    "Ctirad",
    "Cyril",
    "Dalibor",
    "Dalimil",
    "Daniela",
    "Darina",
    "Darja",
    "Denisa",
    "Diana",
    "Dita",
    "Dobromila",
    "Dobroslav",
    "Dominik",
    "Dorota",
    "Doubravka",
    "Drahomíra",
    "Drahoslav",
    "Drahoslava",
    "Dušan",
    "Edita",
    "Eduard",
    "Elena",
    "Eliška",
    "Ema",
    "Emanuel",
    "Emil",
    "Erik",
    "Erika",
    "Evelína",
    "Evženie",
    "Ferdinand",
    "Filip",
    "František",
    "Františka",
    "Gabriel",
    "Gabriela",
    "Gita",
    "Gizela",
    "Gustav",
    "Hana",
    "Hanuš",
    "Havel",
    "Hedvika",
    "Helena",
    "Heřman",
    "Horymír",
    "Hugo",
    "Hynek",
    "Ida",
    "Ignác",
    "Igor",
    "Ilja",
    "Ilona",
    "Ingrid",
    "Irena",
    "Irma",
    "Ivan",
    "Ivana",
    "Iveta",
    "Ivo",
    "Ivona",
    "Izabela",
    "Jáchym",
    "Jakub",
    "Jan",
    "Jana",
    "Jarmil",
    "Jarmila",
    "Jaromír",
    "Jaroslav",
    "Jaroslava",
    "Jeroným",
    "Jindřich",
    "Jindřiška",
    "Jiří",
    "Jiřina",
    "Johana",
    "Jolana",
    "Jonáš",
    "Josef",
    "Julius",
    "Justýna",
    "Kamil",
    "Kamila",
    "Karina",
    "Karolína",
    "Kazimír",
    "Klára",
    "Klaudie",
    "Kristián",
    "Kristýna",
    "Kryštof",
    "Květa",
    "Květoslav",
    "Kvido",
    "Lada",
    "Ladislav",
    "Laura",
    "Lenka",
    "Leona",
    "Leoš",
    "Libor",
    "Liliana",
    "Linda",
    "Ljuba",
    "Lubomír",
    "Lubor",
    "Luboš",
    "Luděk",
    "Ludmila",
    "Ludvík",
    "Lukáš",
    "Lumír",
    "Magdaléna",
    "Marcel",
    "Marcela",
    "Marek",
    "Marián",
    "Mariana",
    "Marie",
    "Marika",
    "Marina",
    "Markéta",
    "Marta",
    "Martina",
    "Matěj",
    "Matouš",
    "Matylda",
    "Maxmilián",
    "Medard",
    "Metoděj",
    "Michaela",
    "Michal",
    "Milada",
    "Milan",
    "Milena",
    "Miloš",
    "Miloslava",
    "Miluše",
    "Miroslav",
    "Miroslava",
    "Mojmír",
    "Monika",
    "Naděžda",
    "Nataša",
    "Nela",
    "Nina",
    "Nora",
    "Norbert",
    "Oldřich",
    "Oldřiška",
    "Oleg",
    "Olga",
    "Oliver",
    "Oskar",
    "Otakar",
    "Oto",
    "Otýlie",
    "Pankrác",
    "Patricie",
    "Patrik",
    "Pavel",
    "Pavla",
    "Pavlína",
    "Petr",
    "Petr",
    "Petra",
    "Pravoslav",
    "Přemysl",
    "Prokop",
    "Radek",
    "Radim",
    "Radka",
    "Radmila",
    "Radomír",
    "Radoslav",
    "Radovan",
    "Regína",
    "Řehoř",
    "Renáta",
    "Richard",
    "Robert",
    "Robin",
    "Roland",
    "Roman",
    "Rostislav",
    "Rudolf",
    "Rút",
    "Růžena",
    "Sabina",
    "Samuel",
    "Sandra",
    "Šárka",
    "Šarlota",
    "Servác",
    "Silvie",
    "Slavěna",
    "Slavomír",
    "Soběslav",
    "Soňa",
    "Stanislav",
    "Stanislava",
    "Štefan",
    "Stela",
    "Štěpánka",
    "Svatava",
    "Svatopluk",
    "Světlana",
    "Tadeáš",
    "Tamara",
    "Taťána",
    "Teodor",
    "Tereza",
    "Tomáš",
    "Václav",
    "Valdemar",
    "Valentýn",
    "Valérie",
    "Vanda",
    "Vavřinec",
    "Věnceslav",
    "Vendelín",
    "Vendula",
    "Věra",
    "Veronika",
    "Věroslav",
    "Viktor",
    "Viktorie",
    "Vilém",
    "Vilma",
    "Vincenc",
    "Viola",
    "Vít",
    "Vítězslav",
    "Vladan",
    "Vladěna",
    "Vladimír",
    "Vladislav",
    "Vlastimil",
    "Vlastislav",
    "Vojtěch",
    "Zbyněk",
    "Zbyšek",
    "Zdeněk",
    "Zdeňka",
    "Zdislava",
    "Zikmund",
    "Zita",
    "Zlata",
    "Zoe",
    "Žofie",
    "Zora",
    "Zuzana",
]
last_names = [
    "Acosta",
    "Adams",
    "Aguilar",
    "Alexander",
    "Allen",
    "Alvarado",
    "Alvarez",
    "Anderson",
    "Andrews",
    "Armstrong",
    "Arnold",
    "Austin",
    "Avila",
    "Bailey",
    "Baker",
    "Banks",
    "Barnes",
    "Barnett",
    "Barrett",
    "Bates",
    "Beck",
    "Bell",
    "Bennett",
    "Berry",
    "Bishop",
    "Black",
    "Bowman",
    "Boyd",
    "Bradley",
    "Brewer",
    "Brooks",
    "Brown",
    "Bryant",
    "Burke",
    "Burns",
    "Burton",
    "Butler",
    "Caldwell",
    "Campbell",
    "Campos",
    "Carlson",
    "Carpenter",
    "Carr",
    "Carroll",
    "Carter",
    "Castillo",
    "Castro",
    "Chambers",
    "Chapman",
    "Chavez",
    "Chen",
    "Clark",
    "Cole",
    "Coleman",
    "Collins",
    "Contreras",
    "Cook",
    "Cooper",
    "Cortez",
    "Cox",
    "Crawford",
    "Cruz",
    "Cunningham",
    "Curtis",
    "Daniels",
    "Davidson",
    "Davis",
    "Day",
    "Dean",
    "Delgado",
    "Diaz",
    "Dixon",
    "Dominguez",
    "Douglas",
    "Duncan",
    "Dunn",
    "Edwards",
    "Elliott",
    "Ellis",
    "Espinoza",
    "Estrada",
    "Evans",
    "Ferguson",
    "Fernandez",
    "Fields",
    "Figueroa",
    "Fisher",
    "Flores",
    "Ford",
    "Foster",
    "Fowler",
    "Fox",
    "Franklin",
    "Freeman",
    "Fuller",
    "Garcia",
    "Gardner",
    "Garrett",
    "Garza",
    "George",
    "Gibson",
    "Gilbert",
    "Gomez",
    "Gonzales",
    "Gonzalez",
    "Gordon",
    "Graham",
    "Grant",
    "Gray",
    "Green",
    "Greene",
    "Griffin",
    "Guerrero",
    "Gutierrez",
    "Guzman",
    "Hall",
    "Hamilton",
    "Hansen",
    "Hanson",
    "Harper",
    "Harris",
    "Harrison",
    "Hart",
    "Harvey",
    "Hawkins",
    "Hayes",
    "Henderson",
    "Henry",
    "Hernandez",
    "Herrera",
    "Hicks",
    "Hill",
    "Hoffman",
    "Holland",
    "Holmes",
    "Hopkins",
    "Howard",
    "Howell",
    "Hudson",
    "Hughes",
    "Hunt",
    "Hunter",
    "Jackson",
    "Jacobs",
    "James",
    "Jenkins",
    "Jensen",
    "Jimenez",
    "Johnson",
    "Johnston",
    "Jones",
    "Jordan",
    "Joseph",
    "Juarez",
    "Keller",
    "Kelley",
    "Kelly",
    "Kennedy",
    "Kim",
    "King",
    "Knight",
    "Lane",
    "Larson",
    "Lawrence",
    "Lawson",
    "Le",
    "Lee",
    "Lewis",
    "Li",
    "Little",
    "Long",
    "Lopez",
    "Lucas",
    "Luna",
    "Lynch",
    "Maldonado",
    "Marquez",
    "Marshall",
    "Martin",
    "Martinez",
    "Mason",
    "Matthews",
    "May",
    "Mccoy",
    "Mcdonald",
    "Medina",
    "Mejia",
    "Mendez",
    "Mendoza",
    "Meyer",
    "Miller",
    "Mills",
    "Mitchell",
    "Molina",
    "Montgomery",
    "Moore",
    "Morales",
    "Moreno",
    "Morgan",
    "Morris",
    "Morrison",
    "Munoz",
    "Murphy",
    "Murray",
    "Myers",
    "Navarro",
    "Nelson",
    "Newman",
    "Nguyen",
    "Nichols",
    "Nunez",
    "Obrien",
    "Oliver",
    "Olson",
    "Ortega",
    "Ortiz",
    "Owens",
    "Padilla",
    "Palmer",
    "Park",
    "Parker",
    "Patel",
    "Patterson",
    "Payne",
    "Pearson",
    "Pena",
    "Perez",
    "Perkins",
    "Perry",
    "Peters",
    "Peterson",
    "Phillips",
    "Pierce",
    "Porter",
    "Powell",
    "Price",
    "Ramirez",
    "Ramos",
    "Ray",
    "Reed",
    "Reid",
    "Reyes",
    "Reynolds",
    "Rice",
    "Richards",
    "Richardson",
    "Riley",
    "Rios",
    "Rivera",
    "Roberts",
    "Robertson",
    "Robinson",
    "Rodriguez",
    "Rogers",
    "Rojas",
    "Romero",
    "Rose",
    "Ross",
    "Ruiz",
    "Russell",
    "Ryan",
    "Salazar",
    "Sanchez",
    "Sanders",
    "Sandoval",
    "Santiago",
    "Santos",
    "Schmidt",
    "Schneider",
    "Schultz",
    "Scott",
    "Shaw",
    "Silva",
    "Simmons",
    "Simpson",
    "Sims",
    "Singh",
    "Smith",
    "Snyder",
    "Soto",
    "Spencer",
    "Stanley",
    "Stephens",
    "Stevens",
    "Stewart",
    "Stone",
    "Sullivan",
    "Taylor",
    "Thomas",
    "Thompson",
    "Torres",
    "Tran",
    "Tucker",
    "Turner",
    "Valdez",
    "Vargas",
    "Vasquez",
    "Vazquez",
    "Vega",
    "Wade",
    "Wagner",
    "Walker",
    "Wallace",
    "Walsh",
    "Wang",
    "Ward",
    "Warren",
    "Washington",
    "Watkins",
    "Watson",
    "Weaver",
    "Webb",
    "Weber",
    "Welch",
    "Wells",
    "West",
    "Wheeler",
    "White",
    "Williams",
    "Williamson",
    "Willis",
    "Wilson",
    "Wong",
    "Wood",
    "Woods",
    "Wright",
    "Yang",
    "Young",
]
titles = [
    "...And It Comes Out Here",
    "2 B R 0 2 B",
    "A Bad Day for Sales",
    "A Book",
    "A Bullet for Cinderella",
    "A history of China",
    "A History of China",
    "A Little Journey",
    "A long way back",
    "A Pail of Air",
    "A Parody Outline of History",
    "A Trace of Memory",
    "Across the Reef: The Marine Assault of Tarawa",
    "All cats are gray",
    "And Devious the Line of Duty",
    "Anthem",
    "Architecture: nineteenth and twentieth centuries",
    "Arm of the Law",
    "Ask a Foolish Question",
    "Astounding Stories of Super-Science",
    "At Suvla Bay",
    "Badge of Infamy",
    "Beyond Lies the Wub",
    "Beyond the Door",
    "Brightside Crossing",
    "Chess Strategy",
    "Closing In: Marines in the Seizure of Iwo Jima",
    "Colonial Homes in North Carolina",
    "Coming Attraction",
    "Computers—the machines we think with",
    "Conjure wife",
    "Crystallizing Public Opinion",
    "Deathworld",
    "Despoilers of the Golden Empire",
    "Down to Earth",
    "Duel on Syrtis",
    "Einstein's planetoid",
    "Facts in Jingles",
    "Fairies and Fusiliers",
    "Forward, Children!",
    "Futuria Fantasia",
    "Gambler's World",
    "Gangway for Homer",
    "Giant brains; or, Machines that think",
    "Great Lent: A School of Repentance",
    "Half loaves",
    "Hellhounds of the Cosmos",
    "Herein is Love",
    "Highways in Hiding",
    "Hypatia; or, woman and knowledge",
    "Industrial Revolution",
    "Inside Earth",
    "Jaywalker",
    "Key Out of Time",
    "Kinematics of Mechanisms from the Time of Watt",
    "Lady into Fox",
    "Left hand, right hand",
    "Legacy",
    "Let's Get Together",
    "Lorelei of the Red Mist",
    "Love Potions Through the Ages: A Study of Amatory Devices and Mores",
    "Mating center",
    "Meet the Tiger",
    "Missing Link",
    "Mr. Spaceship",
    "Old Rambling House",
    "On the Origin of Clockwork, Perpetual Motion Devices, and the Compass",
    "Operation Haystack",
    "Pagan Passions",
    "Pandemic",
    "Papers from Lilliput",
    "Philosophy and the Social Problem",
    "Pillar of Fire",
    "Piper in the Woods",
    "Plague of Pythons",
    "Plague Ship",
    "Plague Ship",
    "Planet of the Damned",
    "Pogo Planet",
    "Prize ship",
    "Project Hush",
    "Rebel Spurs",
    "Referent",
    "Saknarth",
    "Sappho's Journal",
    "Search the Sky",
    "Second Variety",
    "Ships of the seven seas",
    "Some Reptiles and Amphibians from Korea",
    "Space Prison",
    "Star Born",
    "Star Hunter",
    "Static",
    "The atom curtain",
    "The beacon to elsewhere",
    "The Big Trip Up Yonder",
    "The Coffin Cure",
    "The Colors of Space",
    "The Colors of Space",
    "The Common Rocks and Minerals of Missouri",
    "The Creatures That Time Forgot",
    "The Crystal Crypt",
    "The Defenders",
    "The Door Through Space",
    "The Eyes Have It",
    "The Final Campaign: Marines in the Victory on Okinawa",
    "The Floors of the Ocean",
    "The Flying Saucers are Real",
    "The Genetic Effects of Radiation",
    "The Great Implication",
    "The Gun",
    "The Haciendas of Mexico: An Artist's Record",
    "The Hanging Stranger",
    "The Hated",
    "The Highest Treason",
    "The Hobo: The Sociology of the Homeless Man",
    "The Initiates of the Flame",
    "The invading asteroid",
    "The Invisible Government",
    "The Lani People",
    "The men return",
    "The Misplaced Battleship",
    "The Negro in the United States",
    "The Night of the Long Knives",
    "The Power of Sexual Surrender",
    "The programmed people",
    "The Return of the Soldier",
    "The Sex Life of the Gods",
    "The shape of things",
    "The Skull",
    "The Sky Is Falling",
    "The stainless steel rat",
    "The stainless steel rat",
    "The Status Civilization",
    "The Street That Wasn't There",
    "The Time Traders",
    "The Tunnel Under the World",
    "The Variable Man",
    "The Velvet Glove",
    "The Virginia Company Of London",
    "The World of Flying Saucers: A Scientific Examination of a Major Myth of the Space Age",
    "The World That Couldn't Be",
    "They Twinkled Like Jewels",
    "Through the gates of the silver key",
    "Tony and the Beetles",
    "Voices from the Past",
    "Voodoo Planet",
    "Warlord of Kor",
    "Watchbird",
    "Weapon",
    "When the Sun went out",
    "With Lawrence in Arabia",
    "Wolfbane",
    "Worlds Within Worlds: The Story of Nuclear Energy",
    "Young Readers Science Fiction Stories",
    "Youth",
    "Zen and the Art of the Internet",
    "Zero Hour",
]
genres = [
    "action and adventure",
    "autobiography",
    "classic",
    "cookbook",
    "dark academia",
    "domestic fiction",
    "dystopian",
    "essay",
    "fairy tale",
    "fantasy",
    "fiction",
    "graphic novel",
    "historical",
    "horror",
    "magical realism",
    "mystery",
    "nonfiction",
    "poetry",
    "romance",
    "science fiction",
    "thriller",
    "time travel",
]
lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at rhoncus est. Vestibulum non dolor a elit finibus faucibus ut a justo. Sed egestas neque vel lectus fringilla porta. Sed laoreet aliquet sem, et bibendum ligula efficitur cursus. Donec ac placerat mauris. Maecenas eleifend, tortor nec auctor maximus, mi elit molestie tellus, id tincidunt dui tortor sed lorem. Vivamus dolor sapien, egestas ac pulvinar sed, laoreet non dolor. Fusce suscipit rhoncus turpis eu luctus. Nulla imperdiet consectetur diam ut pellentesque. Nam diam mauris, varius a tempor quis, aliquet commodo dui. Nam elementum risus ac neque aliquam elementum. Nullam metus quam, tempus ac tempor vitae, dignissim ac risus. Cras interdum vulputate nisi, non congue justo placerat eu. Donec dictum maximus tortor, a sagittis lacus commodo a. Integer viverra viverra ullamcorper."

import random
import datetime

# initialize Django
import django
import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")
django.setup()

# import our model classes
from diary.models import Genre, Author, Book, Reading


def generate_genres():
    for genre in genres:
        obj = Genre.objects.create(name=genre)
        obj.description = lorem_ipsum
        obj.save()


def generate_authors(count):
    for i in range(count):
        first_name = random.choice(first_names)
        last_name = random.choice(last_names)

        obj = Author.objects.create()
        obj.first_name = first_name
        obj.last_name = last_name
        obj.save()


def generate_books(count):
    for i in range(count):
        title = random.choice(titles)
        issue_date = None
        while issue_date is None:
            try:
                issue_date = datetime.date(
                    year=random.randint(1900, 2000),
                    month=random.randint(1, 13),
                    day=random.randint(1, 32),
                )
            except ValueError:
                pass
        description = " ".join(random.sample(lorem_ipsum.split(), k=50))

        # random selection based on the current number of objects in database
        author_id = random.randint(1, Author.objects.count())
        genre_id = random.randint(1, Genre.objects.count())
        
        # create object in the database
        obj = Book.objects.create(title=title, issue_date=issue_date, description=description, author_id=author_id, genre_id=genre_id)
        obj.save()


def generate_readings(count):
    for i in range(count):
        read_date = None
        while read_date is None:
            try:
                read_date = datetime.date(
                    year=random.randint(2020, 2024),
                    month=random.randint(1, 13),
                    day=random.randint(1, 32),
                )
            except ValueError:
                pass
        book_id = random.randint(1, Book.objects.count())
        obj = Reading.objects.create(book_id=book_id, read_date=read_date)
        obj.save()


generate_genres()
generate_authors(10)
generate_books(10)
generate_readings(10)
