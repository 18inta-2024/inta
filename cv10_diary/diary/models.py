from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.sessions.models import Session

class Page(models.Model):
    path = models.CharField(max_length=1000, unique=True)
    total_visits = models.BigIntegerField(default=0)
    session_visits = models.ManyToManyField(
        Session,
        # volitelné argumenty pro pokročilé chování - on_delete
        through="PageVisit",
        through_fields=("page", "session"),
    )

class PageVisit(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)

class Reading(models.Model):
    book = models.ForeignKey(
        "Book",
        on_delete=models.CASCADE,
    )
    read_date = models.DateField()

    def __str__(self):
        return f"Reading of {self.book} from {self.read_date}"

    def clean(self):
        """Validační metoda pro daný model"""
        if self.read_date < self.book.issue_date:
            raise ValidationError("read_date must be later than book.issue_date")

class Genre(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField(null=True, blank=True) #pole znaku neomezene delky

    def __str__(self):
        return f"{self.name}"

class Author(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class Book(models.Model):
    genre = models.ForeignKey(
       "Genre",
       on_delete=models.CASCADE,
       )
    author = models.ForeignKey(
       "Author",
       on_delete=models.CASCADE,
       )
    title = models.CharField(max_length=50)
    description = models.TextField(null=True, blank=True) #pole znaku neomezene delky
    issue_date = models.DateField()
    cover_image = models.ImageField(upload_to="uploads/book_covers/", null=True, blank=True)

    def __str__(self):
        return f"Book '{self.title}' by {self.author}"

    def short_description(self):
        words = self.description.split()[:10]
        text = " ".join(words).rstrip(".") + "..."
        return text
