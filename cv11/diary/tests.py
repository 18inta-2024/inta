import datetime

from django.test import TestCase
from django.urls import reverse

from .models import Genre, Author, Book, Reading

class GenreModelTests(TestCase):
    def test_str(self):
        g = Genre(name="fantasy", description="bla bla")
        self.assertEqual(str(g), "fantasy")

class AuthorModelTests(TestCase):
    def test_str(self):
        a = Author(first_name="Jan", last_name="Novák")
        self.assertEqual(str(a), "Jan Novák")

class BookModelTests(TestCase):
    def test_str(self):
        g = Genre(name="fantasy", description="bla bla")
        a = Author(first_name="Jan", last_name="Novák")
        book = Book(genre=g, author=a, title="Máj",
                    issue_date=datetime.date.today())
        self.assertEqual(str(book), "Book 'Máj' by Jan Novák")

    def test_short_description_long_text(self):
        """Tests that long description is shortened."""
        g = Genre(name="fantasy", description="bla bla")
        a = Author(first_name="Jan", last_name="Novák")
        description = " ".join(["a"] * 20)
        book = Book(genre=g, author=a, title="Máj",
                    issue_date=datetime.date.today(),
                    description=description)
        self.assertEqual(book.short_description(), "a a a a a a a a a a...")

    def test_short_description_short_text(self):
        """Tests that short description remains unchanged."""
        g = Genre(name="fantasy", description="bla bla")
        a = Author(first_name="Jan", last_name="Novák")
        description = "a b c"
        book = Book(genre=g, author=a, title="Máj",
                    issue_date=datetime.date.today(),
                    description=description)
        self.assertEqual(book.short_description(), "a b c")

    def test_short_description_empty(self):
        """Tests that short_description works for empty description."""
        g = Genre(name="fantasy", description="bla bla")
        a = Author(first_name="Jan", last_name="Novák")
        book = Book(genre=g, author=a, title="Máj",
                    issue_date=datetime.date.today())
        self.assertIsNone(book.description)
        self.assertIsNone(book.short_description())

class IndexViewTests(TestCase):
    def test_index_view_exists(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "diary/index.html")

    def test_index_view_accessible_by_name(self):
        response = self.client.get(reverse("index"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "diary/index.html")

    def test_empty_state(self):
        response = self.client.get(reverse("index"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "diary/index.html")

        #text = "V současné době je zde 0 knih od\n    0 autorů v 0\n    žánrech. Celkový počet přečtení je 0."
        text = "V současné době je zde 0 knih od 0 autorů v 0 žánrech. Celkový počet přečtení je 0."
        self.assertContains(response, text, html=True)

    def test_four_objects(self):
        #g = Genre(name="fantasy", description="bla bla")
        #a = Author(first_name="Jan", last_name="Novák")
        #book = Book(genre=g, author=a, title="Máj",
        #            issue_date=datetime.date.today())
        #reading = Reading(book=book, read_date=datetime.date.today())

        #g.save()
        #a.save()
        #book.save()
        #reading.save()

        g = Genre.objects.create(name="fantasy", description="bla bla")
        a = Author.objects.create(first_name="Jan", last_name="Novák")
        book = Book.objects.create(genre=g, author=a, title="Máj",
                    issue_date=datetime.date.today())
        reading = Reading.objects.create(book=book, read_date=datetime.date.today())

        response = self.client.get(reverse("index"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "diary/index.html")

        text = "V současné době je zde 1 knih od 1 autorů v 1 žánrech. Celkový počet přečtení je 1."
        self.assertContains(response, text, html=True)


class BooksListViewTests(TestCase):
    def test_index_view_exists(self):
        response = self.client.get("/books/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "diary/book_list.html")

    def test_index_view_accessible_by_name(self):
        response = self.client.get(reverse("books_list"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "diary/book_list.html")

    def test_empty_state(self):
        response = self.client.get(reverse("books_list"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "diary/book_list.html")

        text = "<ul></ul>"
        self.assertContains(response, text, html=True)

    def test_one_book(self):
        g = Genre.objects.create(name="fantasy", description="bla bla")
        a = Author.objects.create(first_name="Jan", last_name="Novák")
        book = Book.objects.create(genre=g, author=a, title="Máj",
                    issue_date=datetime.date.today())

        response = self.client.get(reverse("books_list"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "diary/book_list.html")

        text = "Máj"
        self.assertContains(response, text, html=True)


class BookDetailViewTests(TestCase):
    def setUp(self):
        g = Genre.objects.create(name="fantasy", description="bla bla")
        a = Author.objects.create(first_name="Jan", last_name="Novák")
        self.book = Book.objects.create(genre=g, author=a, title="Máj",
                    issue_date=datetime.date.today())

    def test_index_view_accessible_by_name(self):
        response = self.client.get(reverse("book_detail", args=[self.book.id]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "diary/book_detail.html")

    def test_all_attributes_are_shown(self):
        response = self.client.get(reverse("book_detail", args=[self.book.id]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "diary/book_detail.html")

        self.assertContains(response, "Author: Jan Novák")
        self.assertContains(response, "Genre: fantasy")
        ...
