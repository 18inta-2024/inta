from django.shortcuts import render, HttpResponseRedirect, reverse
from django.views.generic import ListView, DetailView
from django.db.models import Count
from django.contrib.auth.decorators import login_required

import datetime

#from diary import models
from . import models
from . import forms

def visit_counter(func):
    def wrapper(request, *args, **kwargs):
        # 1. zpracování požadavku před func

        # nastavení expirace session
        request.session.set_expiry(datetime.timedelta(minutes=10))

        # uložení session v databázi a získání odpovídajícího objektu z databáze
        request.session.save()
        session_db = models.Session.objects.get(session_key=request.session.session_key)

        # vytvoření záznamu pro navštívenou stránku
        # viz https://docs.djangoproject.com/en/4.2/ref/models/querysets/#get-or-create
        page, created = models.Page.objects.get_or_create(path=request.path)
        # přiřazení návštěvy dané stránky
        page.session_visits.add(session_db)
        page.total_visits += 1
        page.save()

        # 2. použití func
        response = func(request, *args, **kwargs)

        # 3. dodatečné zpracování odpovědi

        return response

    return wrapper

@visit_counter
def index(request):
    # set rendering context - variables used in the template
    context = {}
    context["genres_count"] = models.Genre.objects.count()
    context["authors_count"] = models.Author.objects.count()
    context["books_count"] = models.Book.objects.count()
    context["readings_count"] = models.Reading.objects.count()

    # list of the newest books
    context["newest_books"] = models.Book.objects.all().order_by("-issue_date")[:5]
    
    # list of visited pages
    context["pages"] = models.Page.objects.all()

    # render the template
    return render(request, "diary/index.html", context)

class BookDetail(DetailView):
    model = models.Book

class BooksListView(ListView):
    model = models.Book

class GenresListView(ListView):
    model = models.Genre
    queryset = models.Genre.objects.all().annotate(
                    number_of_books=Count("book")
                )

@login_required
def add_author(request):
    # set rendering context - variables used in the template
    context = {}

    # check if there are input data to be processed
    if request.method == "POST":
        # create a form with data from the request
        form = forms.AuthorForm(request.POST)
        # check if the form is valid
        if form.is_valid():
            # create, validate and save the object in database
            new_author = form.save()

            # Always return an HttpResponseRedirect after successfully dealing
            # with POST data. This prevents data from being posted twice if the
            # user hits the <Back> or <Reload> button.
            return HttpResponseRedirect(reverse("index"))

    # create an empty form for GET or any other method
    else:
        form = forms.AuthorForm()

    # continue with normal rendering
    context["authors_count"] = models.Author.objects.count()
    context["form"] = form
    return render(request, "diary/add_author.html", context)

@login_required
def add_book(request):
    # set rendering context - variables used in the template
    context = {}

    # check if there are input data to be processed
    if request.method == "POST":
        # create a form with data from the request
        form = forms.BookForm(request.POST, request.FILES)  # XXX
        # check if the form is valid
        if form.is_valid():
            # create, validate and save the object in database
            form.save()

            # Always return an HttpResponseRedirect after successfully dealing
            # with POST data. This prevents data from being posted twice if the
            # user hits the <Back> or <Reload> button.
            return HttpResponseRedirect(reverse("index"))

    # create an empty form for GET or any other method
    else:
        form = forms.BookForm()

    # continue with normal rendering
    context["books_count"] = models.Book.objects.count()
    context["form"] = form
    return render(request, "diary/add_book.html", context)
