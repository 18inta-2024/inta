from django.db import models

class Page(models.Model):
    path = models.CharField(max_length=1000)

class SessionLogger(models.Model):
    session_id = models.CharField(max_length=16, unique=True)
    expiration = models.DateTimeField()
    visited_pages = models.ManyToManyField(Page)
