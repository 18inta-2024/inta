from django.shortcuts import render
from django.db import IntegrityError
import datetime
import random
from . import models


def first_page(request):
    # zpracování požadavku - vytvoření dokumentu zobrazujícího cookies, které poslal klient
    context = {"cookies": request.COOKIES}
    response = render(request, "pokus/index.html", context)

    # dodatečné zpracování odpovědi - přidání nových cookies
    response.set_cookie("hello", "world")

    for i in range(100):
        key = f"test-{i}"
        if key not in request.COOKIES:
            value = datetime.datetime.utcnow().strftime("%a %d %b %Y, %I:%M%p")
            response.set_cookie(key, value)
            break

    return response




def get_new_session():
    while True:
        try:
            session_id = str(random.randint(0, 10000000))
            exp = datetime.datetime.utcnow() + datetime.timedelta(minutes=10)
            session = models.SessionLogger(session_id=session_id, expiration=exp)
            session.save()
            return session
        except IntegrityError:
            pass

def get_session(session_id=None):
    if session_id:
        session = models.SessionLogger.objects.filter(session_id=session_id).first()
        if session is None:
            return get_new_session()
        else:
            session.expiration = datetime.datetime.utcnow() + datetime.timedelta(minutes=10)
            session.save()
            return session
    else:
        return get_new_session()

def my_cookie_decorator(func):
    def wrapper(request):
        # 1. zpracování požadavku před func
        if "my_session_id" in request.COOKIES:
            session = get_session(request.COOKIES["my_session_id"])
        else:
            session = get_session()

        # TODO: přiřazení návštěvy dané stránky

        # 2. použití func
        response = func(request)

        # 3. dodatečné zpracování odpovědi - přidání nových cookies
        response.set_cookie("my_session_id", session.session_id, expires=session.expiration)

        return response

    return wrapper

@my_cookie_decorator
def another_page(request):
    # zpracování požadavku - vytvoření dokumentu zobrazujícího cookies, které poslal klient
    context = {"cookies": request.COOKIES}
    return render(request, "pokus/index.html", context)

# jiný způsob obalení funkce dekorátorem:
#another_page = my_cookie_decorator(another_page)
