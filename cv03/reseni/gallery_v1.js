var root = document.body;
var data = [];

// komponenta pro zobrazení přehledu všech obrázků
var Gallery = {
    view: function() {
        console.log("Gallery.view");
        return m("main", { class: "gallery-view" },
            [
                m("h1", "Pet Gallery"),
                m("p", data.length + " images"),
                m("section", { id: "gallery-container" },
                    // TODO: vytvořit pole pomocí for cyklu
                    [
                        m("a", { href: "#!/image/0" },
                            m("img", { src: data[0] })
                        )
                    ]
                ),
            ]
        );
    }
}

// komponenta pro zobrazení jednoho obrázku
var Image = {
    view: function(vnode) {
        // first get the parameter from vnode
        id = Number(vnode.attrs.id);
        // TODO: vytvořit správnou strukturu elementů (viz screenshot)
        return m("p", "Image index " + id);
    }
}

m.route(root, "/gallery", {
    "/gallery": Gallery,
    "/image/:id": Image,
})

// obnovení UI po kompletním načtení dat
window.onload = (event) => {
    console.log("window.onload");
    m.redraw();
};
